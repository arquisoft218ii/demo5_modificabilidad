<%--
  Created by IntelliJ IDEA.
  User: tineo
  Date: 21/09/18
  Time: 04:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <link rel="stylesheet" href="css/login.css">

    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simple-keyboard@latest/build/css/index.css">

    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body>

<div class="contenedor">
    <header>

        <h1>SING IN</h1>
        <h6><%=session.getAttribute("block_end") %></h6>
        <h6><%=session.getAttribute("count") %></h6>
    </header>
    <form action="login" class="main" method="post">
        <% if( session.getAttribute("count")!=null && (int) session.getAttribute("count") > 0){ %>
            <% if( (int) session.getAttribute("count") <= 3){ %>
                <h4 style="color: red"> Intentos fallidos: <%=session.getAttribute("count") %></h4>
            <% }else{
                int curr = (int) (java.lang.System.currentTimeMillis() / 1000L);
                int block = 0;
                if(session.getAttribute("block_end")!=null){
                    block = (int) session.getAttribute("block_end");
                }
                int diff =  block - curr;
            %>
                <h4 style="color: red"> Se superaron los 3 intentos fallidos </h4>
                <h4 style="color: red"> Intente en <%=diff %> segundos</h4>
            <% } %>
        <% } %>
        <div class="user">
            <label for="username">Numero de tarjeta: </label>
            <input type="text" id="username" name="username" placeholder="Cuenta">
        </div>

        <div class="password">
            <label for="password">Clave: </label>
            <input type="password" id="password" name="password" id="password" placeholder="Password"  autocomplete="off">
            <div class="simple-keyboard"></div>
        </div>
        <div class="g-recaptcha" data-sitekey="6LdWfHgUAAAAAHAr3IdREE5Z-_y_YuTgdOsOEEW9"></div>
        <button class="btn btn-primary btn-p" >Ingresar</button>
    </form>



</div>
<script src="https://cdn.jsdelivr.net/npm/simple-keyboard@latest/build/index.min.js"></script>
<script>
    let Keyboard = window.SimpleKeyboard.default;

    let myKeyboard = new Keyboard({
        onChange: input => onChange(input),
        onKeyPress: button => onKeyPress(button),
        layout: {
            'default': [
                '1 2 3',
                '4 5 6',
                '7 8 9',
                '0 {bksp} {enter}',
            ],
        },
        //hg-standardBtn
        display: {
            '{bksp}': '⌫',
            '{enter}': '⏎'
        },
        buttonTheme: [
            {
                class: "hg-standardBtn",
                buttons: "{enter} {bksp}"
            }
        ]
    });

    function onChange(input) {
        document.querySelector("#password").value = input;
        console.log("Input changed", input);
    }

    function onKeyPress(button) {
        console.log("Button pressed", button);
    }

</script>

<script>
    $("form").submit(function(event) {

        var recaptcha = $("#g-recaptcha-response").val();
        if (recaptcha === "") {
            event.preventDefault();
            alert("Selecciona el reCaptcha");
        }
    });
</script>

</body>
</html>