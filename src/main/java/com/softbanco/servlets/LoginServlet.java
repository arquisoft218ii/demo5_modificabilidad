package com.softbanco.servlets;

import com.google.gson.Gson;
import com.softbanco.entities.Cliente;
import com.softbanco.services.LoginService;
import org.jboss.logging.Logger;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Consumer;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    final static Logger logger = Logger.getLogger(LoginServlet.class);
    int login_attempts = 3;


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        String userId = request.getParameter("username");
        String password = request.getParameter("password");
        LoginService loginService = new LoginService();
        boolean result = loginService.authenticate(userId, password);
        Cliente user = loginService.getClienteByNumeroTarjeta(userId);
        int countAttempt = 0;
        if(request.getSession().getAttribute("count") != null){
            countAttempt = ((Integer) request.getSession().getAttribute("count")).intValue();
        }
        int block_end = 0;
        int diff = 0;
        if(request.getSession().getAttribute("block_end") != null){
             block_end = ((Integer) request.getSession().getAttribute("block_end")).intValue();
            int curr = (int) (System.currentTimeMillis() / 1000L);
            diff =  block_end - curr;
            if(diff < 0){
                request.getSession().setAttribute("block_end",  0);
                request.getSession().setAttribute("count", 0);
                block_end = 0;
                countAttempt = 0;
            }
        }

        if(result == true && countAttempt < login_attempts && (block_end == 0)){
            request.getSession().setAttribute("user", user);
            request.getSession().setAttribute("count", 0);
            response.sendRedirect("index.jsp");
        }else{
            if(countAttempt < login_attempts){
                request.getSession().setAttribute("count", ++countAttempt);
            }else if( request.getSession().getAttribute("block_end")  == null){
                request.getSession().setAttribute("count", ++countAttempt);
                request.getSession().setAttribute("block_end",  (int) (System.currentTimeMillis() / 1000L)+30);
            }
            logger.error("Login incorrecto: "+ user.getNumero_cuenta());
            response.sendRedirect("login.jsp");
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}